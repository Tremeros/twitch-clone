import React from 'react';
import { Routes } from 'react-router-dom';
import { HistoryRouter } from '../routes/history-router';
import { browserHistory } from '../routes/history';
import routes from '../routes';


export const AppProviders = ({ children }) => {
    return (
        <HistoryRouter history={browserHistory}>
            <Routes>
                {children}
            </Routes>
        </HistoryRouter>
    )

}

const App = () => {
    return (
        <AppProviders>
            {routes}
        </AppProviders>

    )
};

export default App;
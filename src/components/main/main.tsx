import React from 'react';
import { Link } from 'react-router-dom';

export const Main = () => {
    return (
        <>
            <nav>
                <Link to="/">Home</Link>
                <Link to="/login">User</Link>
            </nav>
        </>
    )
}
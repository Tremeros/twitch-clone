import React from 'react';
import { Route, Outlet } from 'react-router-dom';
import { Main } from '../components/main/main';
import { Login } from '../components/main/login';

// export default <>
//     <Route index element={<Main />} />
//     <Route path="/" element={<Main />} />
//     <Route path="login" element={<Login />} />
// </>

export default <Route
    path="/"
    element={
        <>
            <Main />
            <Outlet />
        </>
    }>
    <Route
        path="login"
        element={<Login />} />
</Route>
import React, { ReactNode, useLayoutEffect, useState } from 'react';
import { BrowserHistory } from 'history';
import { Router } from 'react-router-dom';


export interface BrowserRouterProps {
    children?: ReactNode;
    history: BrowserHistory;
    basename?: string
}

export function HistoryRouter({
    children,
    history,
    basename
}: BrowserRouterProps) {
    const [state, setState] = useState({
        action: history.action,
        location: history.location
    });

    useLayoutEffect(() => history.listen(setState), [history]);

    return (
        <Router
            basename={basename}
            children={children}
            location={state.location}
            navigationType={state.action}
            navigator={history} />
    );
}

